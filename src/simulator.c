#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "lifter.h"
#include "simulator.h"
#include "datatypes/metadata.h"
#include "datatypes/matrix.h"
#include "datatypes/status.h"
#include "datatypes/cell.h"

bool isMoveValid(char command, Metadata env, Matrix map, Status stat)
{
    /*
    if (stat.report != MISSION_RUNNING)
        return false;
    */
    
    if (command == 'S')
        return (stat.razors > 0);
    
    if (command == 'W' || command == 'A')
        return true;
    
    int x = stat.robot.x;
    int y = stat.robot.y;
    if (command == 'L')
        x = x - 1;
    else if (command == 'R')
        x = x + 1;
    else if (command == 'U')
        y = y + 1;
    else if (command == 'D')
        y = y - 1;
    else
        return false;
    
    char dest = *cell(map, x, y);
    
    if (dest == ' ' || dest == '.' ||
        dest == '\\' || dest == '!' || dest == 'O')
        return true;
    
    if (dest == '*' && command == 'R'
        && *cell(map, x + 1, y) == ' ')
        return true;
    
    if (dest == '@' && command == 'R'
        && *cell(map, x + 1, y) == ' ')
        return true;
    
    if (dest == '*' && command == 'L'
        && *cell(map, x - 1, y) == ' ')
        return true;
    
    if (dest == '@' && command == 'L'
        && *cell(map, x - 1, y) == ' ')
        return true;
    
    if (dest >= 'A' && dest <= 'I')
        return true;
    
    return false;
}

int moveRobot(char command, Metadata env, Matrix oldMap, Status oldStat, Matrix *newMap, Status *newStat)
{
    
    if (oldStat.report != MISSION_RUNNING) {
        
        int points = -1 * newStat->steps + 25 * newStat->lambdas;
        if (newStat->report == MISSION_ABORTED)
            points += 25 * newStat->lambdas;
        else if (newStat->report == MISSION_COMPLETED)
            points += 50 * newStat->lambdas;
        
        return points;
        
    }
    
    if (command == 'L' || command == 'R'
        || command == 'U' || command == 'D') {
        
        int x = oldStat.robot.x;
        int y = oldStat.robot.y;
        if (command == 'L')
            x = x - 1;
        else if (command == 'R')
            x = x + 1;
        else if (command == 'U')
            y = y + 1;
        else if (command == 'D')
            y = y - 1;
        
        if (x >= 1 && x <= oldMap.width
            && y >= 1 && y <= oldMap.height) {
            
            char dest = *cell(oldMap, x, y);
            
            if (dest == ' ' || dest == '.'
                || dest == '\\' || dest == '!' || dest == 'O') {
                
                newStat->robot.x = x;
                newStat->robot.y = y;
                
                *cell(*newMap, oldStat.robot.x, oldStat.robot.y) = ' ';
                *cell(*newMap, newStat->robot.x, newStat->robot.y) = 'R';
                
                if (dest == '\\')
                    newStat->lambdas++;
                
                if (dest == '!')
                    newStat->razors++;
                
                if (dest == 'O')
                    newStat->report = MISSION_COMPLETED;
                
            } else if (dest == '*' && command == 'R'
                && x + 1 <= oldMap.width
                &&*cell(oldMap, x + 1, y) == ' ') {
                
                newStat->robot.x = x;
                newStat->robot.y = y;
                
                *cell(*newMap, oldStat.robot.x, oldStat.robot.y) = ' ';
                *cell(*newMap, newStat->robot.x, newStat->robot.y) = 'R';
                *cell(*newMap, newStat->robot.x + 1, newStat->robot.y) = '*';
                
            } else if (dest == '@' && command == 'R'
                && x + 1 <= oldMap.width
                &&*cell(oldMap, x + 1, y) == ' ') {
                
                newStat->robot.x = x;
                newStat->robot.y = y;
                
                *cell(*newMap, oldStat.robot.x, oldStat.robot.y) = ' ';
                *cell(*newMap, newStat->robot.x, newStat->robot.y) = 'R';
                *cell(*newMap, newStat->robot.x + 1, newStat->robot.y) = '@';
                
            } else if (dest == '*' && command == 'L'
                && x - 1 >= 1
                && *cell(oldMap, x - 1, y) == ' ') {
                
                newStat->robot.x = x;
                newStat->robot.y = y;
                
                *cell(*newMap, oldStat.robot.x, oldStat.robot.y) = ' ';
                *cell(*newMap, newStat->robot.x, newStat->robot.y) = 'R';
                *cell(*newMap, newStat->robot.x - 1, newStat->robot.y) = '*';
                
            } else if (dest == '@' && command == 'L'
                && x - 1 >= 1
                && *cell(oldMap, x - 1, y) == ' ') {
                
                newStat->robot.x = x;
                newStat->robot.y = y;
                
                *cell(*newMap, oldStat.robot.x, oldStat.robot.y) = ' ';
                *cell(*newMap, newStat->robot.x, newStat->robot.y) = 'R';
                *cell(*newMap, newStat->robot.x - 1, newStat->robot.y) = '@';
                
            } else if (dest >= 'A' && dest <= 'I') {
                
                newStat->robot.x = x;
                newStat->robot.y = y;
                
                *cell(*newMap, oldStat.robot.x, oldStat.robot.y) = ' ';
                *cell(*newMap, newStat->robot.x, newStat->robot.y) = ' ';
                
                Cells *c = getCells(*newMap, env.trampoline[dest - 'A']);
                if (c->size != 1)
                    ERR_ILLEGAL_INPUT
                
                newStat->robot.x = c->elem[0].x;
                newStat->robot.y = c->elem[0].y;
                *cell(*newMap, newStat->robot.x, newStat->robot.y) = 'R';
                
            } else { // wait
                ;
            }
            
        } else { // wait
            ;
        }
        
    } else if (command == 'S' && oldStat.razors > 0) {
        
        int x = oldStat.robot.x;
        int y = oldStat.robot.y;
        
        if (x - 1 > 0 && y - 1 > 0) {
            if (*cell(*newMap, x - 1, y - 1) == 'W')
                *cell(*newMap, x - 1, y - 1) = ' ';
        }
        if (y - 1 > 0) {
            if (*cell(*newMap, x, y - 1) == 'W')
                *cell(*newMap, x, y - 1) = ' ';
        }
        if (y - 1 > 0 && x + 1 <= oldMap.width) {
            if (*cell(*newMap, x + 1, y - 1) == 'W')
                *cell(*newMap, x + 1, y - 1) = ' ';
        }
        if (x - 1 > 0) {
            if (*cell(*newMap, x - 1, y) == 'W')
                *cell(*newMap, x - 1, y) = ' ';
        }
        if (x + 1 <= oldMap.width) {
            if (*cell(*newMap, x + 1, y) == 'W')
                *cell(*newMap, x + 1, y) = ' ';
        }
        if (x - 1 > 0 && y + 1 <= oldMap.height) {
            if (*cell(*newMap, x - 1, y + 1) == 'W')
                *cell(*newMap, x - 1, y + 1) = ' ';
        }
        if (y + 1 <= oldMap.height) {
            if (*cell(*newMap, x, y + 1) == 'W')
                *cell(*newMap, x, y + 1) = ' ';
        }
        if (y + 1 <= oldMap.height && x + 1 <= oldMap.width) {
            if (*cell(*newMap, x + 1, y + 1) == 'W')
                *cell(*newMap, x + 1, y + 1) = ' ';
        }
        
        newStat->razors--;
        
    } else if (command == 'A') {
        
        newStat->report = MISSION_ABORTED;
        
    } else { // wait
        ;
    }
    
    
    
    if (newStat->report == MISSION_ABORTED) {
        
        int points = -1 * newStat->steps + 25 * newStat->lambdas;
        if (newStat->report == MISSION_ABORTED)
            points += 25 * newStat->lambdas;
        else if (newStat->report == MISSION_COMPLETED)
            points += 50 * newStat->lambdas;
        
        return points;
        
    } else if (newStat->report == MISSION_COMPLETED) {
        
        newStat->steps++;
        
        int points = -1 * newStat->steps + 25 * newStat->lambdas;
        if (newStat->report == MISSION_ABORTED)
            points += 25 * newStat->lambdas;
        else if (newStat->report == MISSION_COMPLETED)
            points += 50 * newStat->lambdas;
        
        return points;
        
    } else if (newStat->report == MISSION_RUNNING) {
        
        newStat->steps++;
        
    }
    
    
    
    for (int y = 1; y <= newMap->height; y++) {
        for (int x = 1; x <= newMap->width; x++) {
            
            if (*cell(oldMap, x, y) == '*'
                && y - 1 >= 1
                && *cell(oldMap, x, y - 1) == ' ') {
                
                *cell(*newMap, x, y) = ' ';
                *cell(*newMap, x, y - 1) = '*';
                
            } else if (*cell(oldMap, x, y) == '@'
                && y - 1 >= 1
                && *cell(oldMap, x, y - 1) == ' ') {
                
                *cell(*newMap, x, y) = ' ';
                *cell(*newMap, x, y - 1) = '@';
                
                if (y - 2 >= 1
                    && *cell(oldMap, x, y - 2) != ' ')
                    *cell(*newMap, x, y - 1) = '\\';
                
            } else if (*cell(oldMap, x, y) == '*'
                && y - 1 >= 1
                && (*cell(oldMap, x, y - 1) == '*' || *cell(oldMap, x, y - 1) == '@')
                && x + 1 <= oldMap.width
                && *cell(oldMap, x + 1, y) == ' '
                && *cell(oldMap, x + 1, y - 1) == ' ') {
                
                *cell(*newMap, x, y) = ' ';
                *cell(*newMap, x + 1, y - 1) = '*';
                
            } else if (*cell(oldMap, x, y) == '@'
                && y - 1 >= 1
                && (*cell(oldMap, x, y - 1) == '*' || *cell(oldMap, x, y - 1) == '@')
                && x + 1 <= oldMap.width
                && *cell(oldMap, x + 1, y) == ' '
                && *cell(oldMap, x + 1, y - 1) == ' ') {
                
                *cell(*newMap, x, y) = ' ';
                *cell(*newMap, x + 1, y - 1) = '@';
                
                if (y - 2 >= 1
                    && *cell(oldMap, x + 1, y - 2) != ' ')
                    *cell(*newMap, x + 1, y - 1) = '\\';
                
            } else if (*cell(oldMap, x, y) == '*'
                && y - 1 >= 1
                && (*cell(oldMap, x, y - 1) == '*' || *cell(oldMap, x, y - 1) == '@')
                && x + 1 <= oldMap.width
                && (*cell(oldMap, x + 1, y) != ' ' || *cell(oldMap, x + 1, y - 1) != ' ')
                && x - 1 >= 1
                && *cell(oldMap, x - 1, y) == ' '
                && *cell(oldMap, x - 1, y - 1) == ' ') {
                
                *cell(*newMap, x, y) = ' ';
                *cell(*newMap, x - 1, y - 1) = '*';
                
            } else if (*cell(oldMap, x, y) == '@'
                && y - 1 >= 1
                && (*cell(oldMap, x, y - 1) == '*' || *cell(oldMap, x, y - 1) == '@')
                && x + 1 <= oldMap.width
                && (*cell(oldMap, x + 1, y) != ' ' || *cell(oldMap, x + 1, y - 1) != ' ')
                && x - 1 >= 1
                && *cell(oldMap, x - 1, y) == ' '
                && *cell(oldMap, x - 1, y - 1) == ' ') {
                
                *cell(*newMap, x, y) = ' ';
                *cell(*newMap, x - 1, y - 1) = '@';
                
                if (y - 2 >= 1
                    && *cell(oldMap, x - 1, y - 2) != ' ')
                    *cell(*newMap, x - 1, y - 1) = '\\';
                
            } else if (*cell(oldMap, x, y) == '*'
                && y - 1 >= 1
                && *cell(oldMap, x, y - 1) == '\\'
                && x + 1 <= oldMap.width
                && *cell(oldMap, x + 1, y) == ' '
                && *cell(oldMap, x + 1, y - 1) == ' ') {
                
                *cell(*newMap, x, y) = ' ';
                *cell(*newMap, x + 1, y - 1) = '*';
                
            } else if (*cell(oldMap, x, y) == '@'
                && y - 1 >= 1
                && *cell(oldMap, x, y - 1) == '\\'
                && x + 1 <= oldMap.width
                && *cell(oldMap, x + 1, y) == ' '
                && *cell(oldMap, x + 1, y - 1) == ' ') {
                
                *cell(*newMap, x, y) = ' ';
                *cell(*newMap, x + 1, y - 1) = '@';
                
                if (y - 2 >= 1
                    && *cell(oldMap, x + 1, y - 2) != ' ')
                    *cell(*newMap, x + 1, y - 1) = '\\';
                
            } else if (*cell(oldMap, x, y) == 'L'
                && oldStat.lambdas == env.lambdas) {
                
                *cell(*newMap, x, y) = 'O';
                
            } else if (*cell(oldMap, x, y) == 'W') {
                
                if ((oldStat.steps + 1) % env.growth == 0) {
                    
                    if (x - 1 > 0 && y - 1 > 0) {
                        if (*cell(*newMap, x - 1, y - 1) == ' ')
                            *cell(*newMap, x - 1, y - 1) = 'W';
                    }
                    if (y - 1 > 0) {
                        if (*cell(*newMap, x, y - 1) == ' ')
                            *cell(*newMap, x, y - 1) = 'W';
                    }
                    if (y - 1 > 0 && x + 1 <= oldMap.width) {
                        if (*cell(*newMap, x + 1, y - 1) == ' ')
                            *cell(*newMap, x + 1, y - 1) = 'W';
                    }
                    if (x - 1 > 0) {
                        if (*cell(*newMap, x - 1, y) == ' ')
                            *cell(*newMap, x - 1, y) = 'W';
                    }
                    if (x + 1 <= oldMap.width) {
                        if (*cell(*newMap, x + 1, y) == ' ')
                            *cell(*newMap, x + 1, y) = 'W';
                    }
                    if (x - 1 > 0 && y + 1 <= oldMap.height) {
                        if (*cell(*newMap, x - 1, y + 1) == ' ')
                            *cell(*newMap, x - 1, y + 1) = 'W';
                    }
                    if (y + 1 <= oldMap.height) {
                        if (*cell(*newMap, x, y + 1) == ' ')
                            *cell(*newMap, x, y + 1) = 'W';
                    }
                    if (y + 1 <= oldMap.height && x + 1 <= oldMap.width) {
                        if (*cell(*newMap, x + 1, y + 1) == ' ')
                            *cell(*newMap, x + 1, y + 1) = 'W';
                    }
                    
                }
                
            } else { // unchanged
                ;
            }
            
        }
    }
    
    if (newStat->robot.y + 1 <= oldMap.height
        && (*cell(*newMap, newStat->robot.x, newStat->robot.y + 1) == '*'
        || *cell(*newMap, newStat->robot.x, newStat->robot.y + 1) == '@')
        && *cell(oldMap, newStat->robot.x, newStat->robot.y + 1) == ' ') {
        
        newStat->report = MISSION_FAILED;
        
        int points = -1 * newStat->steps + 25 * newStat->lambdas;
        if (newStat->report == MISSION_ABORTED)
            points += 25 * newStat->lambdas;
        else if (newStat->report == MISSION_COMPLETED)
            points += 50 * newStat->lambdas;
        
        return points;
        
    }
    
    if (env.flooding) {
        int water_level = env.water + newStat->steps / env.flooding;
        if (newStat->robot.y <= water_level) {
            
            newStat->underwater_time++;
            if (newStat->underwater_time > env.waterproof) {
                
                newStat->report = MISSION_FAILED;
                
                int points = -1 * newStat->steps + 25 * newStat->lambdas;
                if (newStat->report == MISSION_ABORTED)
                    points += 25 * newStat->lambdas;
                else if (newStat->report == MISSION_COMPLETED)
                    points += 50 * newStat->lambdas;
                
                return points;
                
            }
            
        } else {
            newStat->underwater_time = 0;
        }
    }
    
    int points = -1 * newStat->steps + 25 * newStat->lambdas;
    if (newStat->report == MISSION_ABORTED)
        points += 25 * newStat->lambdas;
    else if (newStat->report == MISSION_COMPLETED)
        points += 50 * newStat->lambdas;
    
    return points;
}

int simulate(const char *commands, Metadata env, Matrix initMap, Status initStat, bool doPrint)
{
    Matrix *map1 = cloneMatrix(initMap), *map2;
    Status *stat1 = cloneStatus(initStat), *stat2;
    
    if (doPrint) {
        printMatrix(*map1);
        printStatus(*stat1);
        printf("\n");
    }
    
    for (int i = 0; i < strlen(commands) && stat1->report == MISSION_RUNNING; i++) {
        
        map2 = cloneMatrix(*map1);
        stat2 = cloneStatus(*stat1);
        
        moveRobot(commands[i], env, *map1, *stat1, map2, stat2);
        
        destructMatrix(*map1);
        free(map1);
        free(stat1);
        
        map1 = cloneMatrix(*map2);
        stat1 = cloneStatus(*stat2);
        
        destructMatrix(*map2);
        free(map2);
        free(stat2);
        
        if (doPrint) {
            printMatrix(*map1);
            printStatus(*stat1);
            printf("\n");
        }
        
    }
    
    int points = -1 * stat1->steps + 25 * stat1->lambdas;
    if (stat1->report == MISSION_ABORTED)
        points += 25 * stat1->lambdas;
    else if (stat1->report == MISSION_COMPLETED)
        points += 50 * stat1->lambdas;
    
    destructMatrix(*map1);
    free(map1);
    free(stat1);
    
    return points;
}

int simulateRobot(const char *commands, Metadata env, Matrix initMap, Status initStat, Matrix **newMap, Status **newStat)
{
    Matrix *map1 = cloneMatrix(initMap), *map2;
    Status *stat1 = cloneStatus(initStat), *stat2;
    
    for (int i = 0; i < strlen(commands) && stat1->report == MISSION_RUNNING; i++) {
        
        map2 = cloneMatrix(*map1);
        stat2 = cloneStatus(*stat1);
        
        moveRobot(commands[i], env, *map1, *stat1, map2, stat2);
        
        destructMatrix(*map1);
        free(map1);
        free(stat1);
        
        map1 = cloneMatrix(*map2);
        stat1 = cloneStatus(*stat2);
        
        destructMatrix(*map2);
        free(map2);
        free(stat2);
        
    }
    
    int points = -1 * stat1->steps + 25 * stat1->lambdas;
    if (stat1->report == MISSION_ABORTED)
        points += 25 * stat1->lambdas;
    else if (stat1->report == MISSION_COMPLETED)
        points += 50 * stat1->lambdas;
    
    *newMap = map1;
    *newStat = stat1;
    
    return points;
}
