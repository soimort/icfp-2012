#include <stdio.h>
#include <stdlib.h>

#include "status.h"
#include "../lifter.h"

void printStatus(Status s)
{
    printf("STEP: #%d\n", s.steps);
    printf("Robot: (%d, %d)\n", s.robot.x, s.robot.y);
    printf("Razors: %d\n", s.razors);
    printf("Lambdas: %d\n", s.lambdas);
    printf("Underwater time: %d\n", s.underwater_time);
    printf("Report: %d\n", s.report);
    printf("\n");
    
    return;
}

Status *cloneStatus(Status s)
{
    Status *t = malloc(sizeof(Status));
    if (t == NULL)
        ERR_OUT_OF_MEMORY
    
    t->robot.x = s.robot.x;
    t->robot.y = s.robot.y;
    t->razors = s.razors;
    t->steps = s.steps;
    t->lambdas = s.lambdas;
    t->underwater_time = s.underwater_time;
    t->report = s.report;
    
    return t;
}
