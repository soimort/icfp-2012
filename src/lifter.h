#ifndef LIFTER
#define LIFTER

#include <stdio.h>
#include <stdlib.h>

#define MAX_WIDTH 10001

#define ERR_OUT_OF_MEMORY { fprintf(stderr, "FATAL ERROR: out of memory\n"); exit(EXIT_FAILURE); }
#define ERR_ILLEGAL_INPUT { fprintf(stderr, "FATAL ERROR: illegal input\n"); exit(EXIT_FAILURE); }

#define MISSION_REPORT int
#define MISSION_RUNNING 0
#define MISSION_COMPLETED 1
#define MISSION_ABORTED 2
#define MISSION_FAILED 3

#endif /* LIFTER */
