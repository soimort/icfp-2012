#ifndef DATATYPE_METADATA
#define DATATYPE_METADATA

typedef struct {
    int water;
    int flooding;
    int waterproof;
    int growth;
    int razors;
    int lambdas;
    char trampoline[10];
} Metadata;

void printMetadata(Metadata);

#endif /* DATATYPE_METADATA */
