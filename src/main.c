#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "io.h"
#include "core.h"
#include "simulator.h"
#include "datatypes/metadata.h"
#include "datatypes/matrix.h"
#include "datatypes/status.h"
#include "datatypes/cell.h"

void mapTesting(Metadata env, Matrix map, Status stat)
{
    printf("Width: %d\n", map.width);
    printf("Height: %d\n\n", map.height);
    printMetadata(env);
    printf("\n");
    
    printf(">> %d\n", simulate("", env, map, stat, true));
    
    return;
}

int main()
{
    Metadata env = {0, 0, 10, 25, 0, 0, {}};
    Matrix map = {0, 0, NULL};
    Status stat = {{0, 0}, 0, 0, 0, 0, MISSION_RUNNING};
    
    readStdIn(&env, &map, &stat);
    //mapTesting(env, map, stat); // for testing
    
    int score;
    char *solution = malloc((map.width * map.height) * sizeof(char));
    memset(solution, 0, strlen(solution));
    
    score = core_bb(solution, env, map, stat, 140);
    
    printf("%s\n", solution);
    printf("%d\n", score); // for testing
    
    free(solution);
    
    destructMatrix(map);
    
    return 0;
}
