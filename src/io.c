#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "io.h"
#include "lifter.h"
#include "datatypes/metadata.h"
#include "datatypes/matrix.h"
#include "datatypes/status.h"
#include "datatypes/cell.h"

void readStdIn(Metadata *env, Matrix *map, Status *stat)
{
    char temp[MAX_WIDTH];
    fgets(temp, MAX_WIDTH, stdin);
    
    map->height = 0;
    map->elem = malloc(++map->height * sizeof(char *));
    if (map->elem == NULL)
        ERR_OUT_OF_MEMORY
    
    map->width = strlen(temp) - 1;
    map->elem[map->height - 1] = malloc((map->width + 2) * sizeof(char));
    if (map->elem[map->height - 1] == NULL)
        ERR_OUT_OF_MEMORY
    strcpy(map->elem[map->height - 1], temp);
    
    fgets(temp, MAX_WIDTH, stdin);
    
    while (temp != NULL && temp[0] != '\n') {
        char **p = realloc(map->elem, ++map->height * sizeof(char *));
        if (p == NULL)
            ERR_OUT_OF_MEMORY
        map->elem = p;
        
        if (strlen(temp) - 1 > map->width)
            map->width = strlen(temp) - 1;
        map->elem[map->height - 1] = malloc((map->width + 2) * sizeof(char));
        if (map->elem[map->height - 1] == NULL)
            ERR_OUT_OF_MEMORY
        strcpy(map->elem[map->height - 1], temp);
        
        if (fgets(temp, MAX_WIDTH, stdin) == NULL)
            break;
    }
    
    while (fgets(temp, 64, stdin)) {
        char metadata_n[16] = "", metadata_k = 0, metadata_v = 0;
        
        if (sscanf(temp, "Trampoline %c%*s %c", &metadata_k, &metadata_v) != 0)
            env->trampoline[metadata_k - 'A'] = metadata_v;
        else if (sscanf(temp, "Water %d", &metadata_v) != 0)
            env->water = metadata_v;
        else if (sscanf(temp, "Flooding %d", &metadata_v) != 0)
            env->flooding = metadata_v;
        else if (sscanf(temp, "Waterproof %d", &metadata_v) != 0)
            env->waterproof = metadata_v;
        else if (sscanf(temp, "Growth %d", &metadata_v) != 0)
            env->growth = metadata_v;
        else if (sscanf(temp, "Razors %d", &metadata_v) != 0)
            env->razors = metadata_v;
    }
    
    Cells *c = getCells(*map, '\\');
    env->lambdas = c->size;
    
    c = getCells(*map, '@');
    env->lambdas += c->size;
    
    c = getCells(*map, 'R');
    if (c->size != 1)
        ERR_ILLEGAL_INPUT
    
    stat->robot.x = c->elem[0].x;
    stat->robot.y = c->elem[0].y;
    stat->razors = env->razors;
    
    free(c);
    
    return;
}
