#ifndef DATATYPE_MATRIX
#define DATATYPE_MATRIX

typedef struct {
    int width, height;
    char **elem;
} Matrix;

void printMatrix(Matrix);

void destructMatrix(Matrix);

Matrix *cloneMatrix(Matrix);

#endif /* DATATYPE_MATRIX */
