#ifndef LIFTER_SIMULATOR
#define LIFTER_SIMULATOR

#include <stdbool.h>

#include "datatypes/metadata.h"
#include "datatypes/matrix.h"
#include "datatypes/status.h"

bool isMoveValid(char command, Metadata, Matrix, Status);

int moveRobot(char command, Metadata env, Matrix oldMap, Status oldStat, Matrix *newMap, Status *newStat);

int simulate(const char *commands, Metadata, Matrix initMap, Status initStat, bool doPrint);

int simulateRobot(const char *commands, Metadata env, Matrix initMap, Status initStat, Matrix **newMap, Status **newStat);

#endif /* LIFTER_SIMULATOR */
