#include <stdlib.h>
#include <string.h>

#include "cell.h"
#include "matrix.h"
#include "../lifter.h"

char *cell(Matrix m, int x, int y)
{
    return &m.elem[m.height - y][x - 1];
}

Cells *getCells(Matrix m, char ch)
{
    int size = 0;
    Cells *c = malloc(1 * sizeof(Cells));
    c->size = size;
    c->elem = NULL;
    
    for (int i = 0; i < m.height; i++)
        for (int j = 0; j < m.width && j < strlen(m.elem[i]); j++)
            if (m.elem[i][j] == ch) {
                Cell *p = realloc(c->elem, ++size * sizeof(Cell));
                if (p == NULL)
                    ERR_OUT_OF_MEMORY
                c->elem = p;
                
                c->size = size;
                c->elem[size - 1].x = j + 1;
                c->elem[size - 1].y = m.height - i;
            }
    
    return c;
}
