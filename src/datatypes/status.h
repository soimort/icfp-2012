#ifndef DATATYPE_STATUS
#define DATATYPE_STATUS

#include "cell.h"
#include "../lifter.h"

typedef struct {
    Cell robot;
    int razors;
    int steps;
    int lambdas;
    int underwater_time;
    MISSION_REPORT report;
} Status;

void printStatus(Status);

Status *cloneStatus(Status);

#endif /* DATATYPE_STATUS */
