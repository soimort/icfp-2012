#include <stdio.h>

#include "metadata.h"

void printMetadata(Metadata d)
{
    printf("Water: %d\n", d.water);
    printf("Flooding: %d\n", d.flooding);
    printf("Waterproof: %d\n", d.waterproof);
    printf("Growth: %d\n", d.growth);
    printf("Razors: %d\n", d.razors);
    printf("Lambdas: %d\n", d.lambdas);
    
    printf("Trampoline: \n");
    for (int i = 0; i < 9; i++)
        if (d.trampoline[i])
            printf("  %c <- %c\n", d.trampoline[i], 'A' + i);
    
    printf("\n");
    
    return;
}
