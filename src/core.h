#ifndef LIFTER_CORE
#define LIFTER_CORE

#include "datatypes/metadata.h"
#include "datatypes/matrix.h"
#include "datatypes/status.h"

int core_british_museum(char *solution, Metadata, Matrix, Status, int time_limit);

int core_bb(char *solution, Metadata, Matrix, Status, int time_limit);

#endif /* LIFTER_CORE */
