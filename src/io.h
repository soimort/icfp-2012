#ifndef LIFTER_IO
#define LIFTER_IO

#include "datatypes/metadata.h"
#include "datatypes/matrix.h"
#include "datatypes/status.h"

void readStdIn(Metadata *, Matrix *, Status *);

#endif /* LIFTER_IO */
