#include <string.h>
#include <time.h>

#include "../core.h"
#include "../simulator.h"

typedef struct node {
    char *str;
    struct node *next;
} Queue;

int core_bb(char *solution, Metadata env, Matrix map, Status stat, int time_limit)
{
    int full_score = 75 * env.lambdas;
    
    int best_score = 0;
    char *best_solution = malloc((map.width * map.height) * sizeof(char));
    memset(best_solution, 0, strlen(best_solution));
    
    int score = 0;
    char *attempt = malloc((map.width * map.height) * sizeof(char));
    memset(attempt, 0, strlen(attempt));
    
    Matrix *newMap;
    Status *newStat;
    
    Queue *head = malloc(1 * sizeof(Queue));
    Queue *tail = head;
    
    head->str = malloc((map.width * map.height) * sizeof(char));
    strcpy(head->str, "");
    head->next = NULL;
    
    while (head != NULL && clock() / CLOCKS_PER_SEC < time_limit) {
        
        strcpy(attempt, head->str);
        strcat(attempt, "A");
        
        newMap = cloneMatrix(map);
        newStat = cloneStatus(stat);
        score = simulateRobot(attempt, env, map, stat, &newMap, &newStat);
        
        if (score > best_score) {
            best_score = score;
            
            if (newStat->report == MISSION_ABORTED)
                strcpy(best_solution, attempt);
            else
                strcpy(best_solution, head->str);
        }
        
        if (newStat->report != MISSION_COMPLETED
            && newStat->report != MISSION_FAILED) {
            
            if (isMoveValid('R', env, *newMap, *newStat)) {
                strcpy(attempt, head->str);
                strcat(attempt, "R");
                tail = tail->next = malloc(1 * sizeof(Queue));
                if (tail == NULL) break;
                tail->str = malloc((map.width * map.height) * sizeof(char));
                if (tail->str == NULL) break;
                strcpy(tail->str, attempt);
                tail->next = NULL;
            }
            
            if (isMoveValid('L', env, *newMap, *newStat)) {
                strcpy(attempt, head->str);
                strcat(attempt, "L");
                tail = tail->next = malloc(1 * sizeof(Queue));
                if (tail == NULL) break;
                tail->str = malloc((map.width * map.height) * sizeof(char));
                if (tail->str == NULL) break;
                strcpy(tail->str, attempt);
                tail->next = NULL;
            }
            
            if (isMoveValid('D', env, *newMap, *newStat)) {
                strcpy(attempt, head->str);
                strcat(attempt, "D");
                tail = tail->next = malloc(1 * sizeof(Queue));
                if (tail == NULL) break;
                tail->str = malloc((map.width * map.height) * sizeof(char));
                if (tail->str == NULL) break;
                strcpy(tail->str, attempt);
                tail->next = NULL;
            }
            
            if (isMoveValid('U', env, *newMap, *newStat)) {
                strcpy(attempt, head->str);
                strcat(attempt, "U");
                tail = tail->next = malloc(1 * sizeof(Queue));
                if (tail == NULL) break;
                tail->str = malloc((map.width * map.height) * sizeof(char));
                if (tail->str == NULL) break;
                strcpy(tail->str, attempt);
                tail->next = NULL;
            }
            
            if (isMoveValid('S', env, *newMap, *newStat)) {
                strcpy(attempt, head->str);
                strcat(attempt, "S");
                tail = tail->next = malloc(1 * sizeof(Queue));
                if (tail == NULL) break;
                tail->str = malloc((map.width * map.height) * sizeof(char));
                if (tail->str == NULL) break;
                strcpy(tail->str, attempt);
                tail->next = NULL;
            }
            
            /*
            strcpy(attempt, head->str);
            strcat(attempt, "W");
            tail = tail->next = malloc(1 * sizeof(Queue));
            if (tail == NULL) break;
            tail->str = malloc((map.width * map.height) * sizeof(char));
            if (tail->str == NULL) break;
            strcpy(tail->str, attempt);
            tail->next = NULL;
            */
            
        }
        
        Queue *temp = head->next;
        free(head->str);
        free(head);
        head = temp;
        
        destructMatrix(*newMap);
        free(newMap);
        free(newStat);
        
        if (best_score == full_score - strlen(head->str))
            break;
    }
    
    while (head != NULL) {
        Queue *temp = head->next;
        free(head->str);
        free(head);
        head = temp;
    }
    
    free(attempt);
    
    strcpy(solution, best_solution);
    free(best_solution);
    
    return best_score;
}
