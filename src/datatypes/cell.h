#ifndef DATATYPE_CELL
#define DATATYPE_CELL

#include "matrix.h"

typedef struct {
    int x;
    int y;
    int val;
} Cell;

typedef struct {
    int size;
    Cell *elem;
} Cells;

char *cell(Matrix, int x, int y);

Cells *getCells(Matrix, char);

#endif /* DATATYPE_CELL */
