#include <string.h>
#include <time.h>

#include "../core.h"
#include "../simulator.h"

int core_british_museum(char *solution, Metadata env, Matrix map, Status stat, int time_limit)
{
    int full_score = 75 * env.lambdas;
    
    int best_score = 0;
    char *best_solution = malloc((map.width * map.height) * sizeof(char));
    memset(best_solution, 0, strlen(best_solution));
    
    int score = 0;
    char *attempt = malloc((map.width * map.height) * sizeof(char));
    memset(attempt, 0, strlen(attempt));
    
    int digits = 0;
    while (++digits > 0 && clock() / CLOCKS_PER_SEC < time_limit) {
        int d = 0, temp;
        do {
            memset(attempt, 0, strlen(attempt));
            memset(attempt, 'R', digits);
            
            temp = d;
            for (int i = 0; temp && i < digits; i++) {
                switch (temp % 6) {
                    case 0:
                        attempt[i] = 'R'; break;
                    case 1:
                        attempt[i] = 'L'; break;
                    case 2:
                        attempt[i] = 'D'; break;
                    case 3:
                        attempt[i] = 'U'; break;
                    case 4:
                        attempt[i] = 'S'; break;
                    case 5:
                        attempt[i] = 'W'; break;
                }
                temp /= 6;
            }
            
            if (temp)
                break;
            
            score = simulate(attempt, env, map, stat, false);
            if (score > best_score) {
                best_score = score;
                strcpy(best_solution, attempt);
            }
            
            strcat(attempt, "A");
            score = simulate(attempt, env, map, stat, false);
            if (score > best_score) {
                best_score = score;
                strcpy(best_solution, attempt);
            }
            
            if (best_score == full_score - digits)
                break;
        } while (++d > 0 && clock() / CLOCKS_PER_SEC < time_limit);
        
        if (best_score == full_score - digits)
            break;
    }
    
    free(attempt);
    
    strcpy(solution, best_solution);
    free(best_solution);
    
    return best_score;
}
