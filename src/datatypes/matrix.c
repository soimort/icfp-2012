#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "matrix.h"
#include "../lifter.h"

void printMatrix(Matrix m)
{
    for (int i = 0; i < m.height; i++)
        printf("%s", m.elem[i]);
    printf("\n");
    
    return;
}

void destructMatrix(Matrix m)
{
    for (int i = 0; i < m.height; i++)
        free(m.elem[i]);
    free(m.elem);
    
    return;
}

Matrix *cloneMatrix(Matrix m)
{
    Matrix *n = malloc(sizeof(Matrix));
    if (n == NULL)
        ERR_OUT_OF_MEMORY
    
    n->width = m.width;
    n->height = m.height;
    n->elem = malloc(m.height * sizeof(char *));
    if (n->elem == NULL)
        ERR_OUT_OF_MEMORY
    
    for (int i = 0; i < m.height; i++) {
        n->elem[i] = malloc((m.width + 2) * sizeof(char));
        if (n->elem[i] == NULL)
            ERR_OUT_OF_MEMORY
        
        strcpy(n->elem[i], m.elem[i]);
    }
    
    return n;
}
